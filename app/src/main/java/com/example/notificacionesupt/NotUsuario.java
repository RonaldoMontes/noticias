package com.example.notificacionesupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.notificacionesupt.Api.Api;
import com.example.notificacionesupt.Api.Servicios.ServicioPeticion;
import com.example.notificacionesupt.ViewModels.Peticion_Mostrar;
import com.example.notificacionesupt.ViewModels.Peticion_RegNot;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotUsuario extends AppCompatActivity {
    private EditText usid, ti, ds;
    private Button Reg, Bac;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not_usuario);
        usid = (EditText)findViewById(R.id.editText9);
        ti = (EditText)findViewById(R.id.editText10);
        ds = (EditText)findViewById(R.id.editText11);
        Reg = (Button)findViewById(R.id.button10);
        Bac = (Button) findViewById(R.id.button11);

        Bac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NotUsuario.this, Menu.class);

                startActivity(intent);
            }
        });
        Reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServicioPeticion s = Api.getApi(NotUsuario.this).create(ServicioPeticion.class);
                int ids = Integer.parseInt(usid.getText().toString());
                Call<Peticion_RegNot> Registros = s.getNotUsu(ids, ti.getText().toString(), ds.getText().toString());
                Registros.enqueue(new Callback<Peticion_RegNot>() {
                    @Override
                    public void onResponse(Call<Peticion_RegNot> call, Response<Peticion_RegNot> response) {
                        Peticion_RegNot mostrar = response.body();
                        String Cadena = "";
                        if (mostrar.estado.equals("true")){

                            Toast.makeText(NotUsuario.this, "Registro Exitoso", Toast.LENGTH_SHORT).show();

                        }else{
                            Toast.makeText(NotUsuario.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_RegNot> call, Throwable t) {
                        Toast.makeText(NotUsuario.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
