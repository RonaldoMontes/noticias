package com.example.notificacionesupt.ViewModels;

import com.example.notificacionesupt.Noti;

import java.util.List;

public class Peticion_Detalle {
    public String estado;
    public List<Noti> procesador;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Noti> getProcesador() {
        return procesador;
    }

    public void setProcesador(List<Noti> procesador) {
        this.procesador = procesador;
    }
}
