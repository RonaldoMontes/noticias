package com.example.notificacionesupt.ViewModels;

import com.example.notificacionesupt.Noti;

import java.util.List;

public class Peticion_Mostrar {
    public String estado;
    public List<Noti> detalle;



    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Noti> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<Noti> detalle) {
        this.detalle = detalle;
    }
}
