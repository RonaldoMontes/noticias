package com.example.notificacionesupt.Api.Servicios;

import com.example.notificacionesupt.ViewModels.Peticion_Detalle;
import com.example.notificacionesupt.ViewModels.Peticion_Login;
import com.example.notificacionesupt.ViewModels.Peticion_Mostrar;
import com.example.notificacionesupt.ViewModels.Peticion_RegNot;
import com.example.notificacionesupt.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    abstract Call<Registro_Usuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);


    @GET("api/todasNot")
    Call<Peticion_Mostrar> getNoticia();

    @FormUrlEncoded
    @POST("api/crearNotUsuario")
    Call<Peticion_RegNot> getNotUsu(@Field("usuarioId") int id, @Field ("titulo") String tit, @Field("descripcion") String des);
}
